package data.structures;

public class LockAtomicInteger {

    private volatile int integer;
    private static MyLock lock;

    static {
        lock = new MyLock();
    }

    public LockAtomicInteger() {
        integer = 0;
    }

    public LockAtomicInteger(int initialVal) {
        integer = initialVal;
    }

    public final int get() {
        return integer;
    }

    public final void set(int newValue) {
        integer = newValue;
    }

    public final int getAndSet(int newValue) throws InterruptedException {
        lock.lock();
        try {
            int old = integer;
            set(newValue);
            return old;
        } finally {
            lock.unlock();
        }
    }

    public final boolean compareAndSet(int expect, int update)
            throws InterruptedException {
        lock.lock();
        try {
            if (integer != expect) {
                return false;
            }
            set(update);
            return true;
        } finally {
            lock.unlock();
        }
    }

    public final int getAndIncrement() throws InterruptedException {
        lock.lock();
        try {
            int old = integer;
            ++integer;
            return old;
        } finally {
            lock.unlock();
        }
    }

    public final int getAndDecrement() throws InterruptedException {
        lock.lock();
        try {
            int old = integer;
            --integer;
            return old;
        } finally {
            lock.unlock();
        }
    }

    public final int getAndAdd(int delta) throws InterruptedException {
        lock.lock();
        try {
            int old = integer;
            integer += delta;
            return old;
        } finally {
            lock.unlock();
        }
    }

    public final int incrementAndGet() throws InterruptedException {
        lock.lock();
        try {
            ++integer;
            return integer;
        } finally {
            lock.unlock();
        }
    }

    public final int decrementAndGet() throws InterruptedException {
        lock.lock();
        try {
            --integer;
            return integer;
        } finally {
            lock.unlock();
        }
    }

    public final int addAndGet(int delta) throws InterruptedException {
        lock.lock();
        try {
            integer += delta;
            return integer;
        } finally {
            lock.unlock();
        }
    }

    public int intValue() {
        return integer;
    }

    public synchronized long longValue() {
        return (long) integer;
    }

    public synchronized float floatValue() {
        return (float) integer;
    }

    public synchronized double doubleValue() {
        return (double) integer;
    }

    public synchronized byte byteValue() {
        return (byte) integer;
    }

    public String toString() {
        return Integer.toString(integer);
    }
}
