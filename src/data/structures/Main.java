package data.structures;

import java.util.Calendar;

import data.structures.tools.ParallelCounter;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        long start = Calendar.getInstance().getTimeInMillis();
        ParallelCounter c = new ParallelCounter();
        System.out.println(c.getOddNumbers());
        long end = Calendar.getInstance().getTimeInMillis();
        System.out.println(((end - start) * 0.001) + " sec");
    }
}
