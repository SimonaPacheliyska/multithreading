package data.structures.tools;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class ParallelCounter {

    private static int TOTAL_COUNT = 2_000_000_000;
    private static int CHUNK_SIZE = 250_000_000;
    private static int POOL_SIZE = TOTAL_COUNT / CHUNK_SIZE;

    private ExecutorService executor;
    private volatile int cntr;
    private AtomicInteger finished;

    public ParallelCounter() {
        executor = Executors.newFixedThreadPool(POOL_SIZE);
        cntr = 0;
        finished = new AtomicInteger(0);
        proceed();
    }

    public static boolean isOdd(int num) {
        return (num % 2 != 0);
    }

    private void add(long occ) {
        synchronized (this) {
            cntr += occ;
            notifyAll();
        }
    }

    public int getOddNumbers() throws InterruptedException {
        synchronized (this) {
            while (finished.get() < POOL_SIZE) {
                wait();
            }
            executor.shutdown();
            return cntr;
        }

    }

    private void proceed() {
        for (int i = 0; i < POOL_SIZE; ++i) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    add(IntStream.range(0, CHUNK_SIZE).parallel()
                            .filter(ParallelCounter::isOdd).count());
                    finished.incrementAndGet();
                }
            });
        }
    }

}
