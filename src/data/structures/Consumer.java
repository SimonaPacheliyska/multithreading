package data.structures;

public interface Consumer<E> {
    void consume() throws InterruptedException;

}
