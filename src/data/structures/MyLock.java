package data.structures;

public class MyLock {
    private boolean isLocked;

    public MyLock() {
        isLocked = false;
    }

    public synchronized void lock() throws InterruptedException {
        while (isLocked) {
            wait();
        }
        isLocked = true;
    }

    public synchronized void unlock() throws InterruptedException {
        isLocked = false;
        notifyAll();
    }

}
