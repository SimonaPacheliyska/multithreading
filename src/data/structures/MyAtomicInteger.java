package data.structures;

public class MyAtomicInteger {
    private int number;

    public MyAtomicInteger() {
        number = 0;
    }

    public MyAtomicInteger(int intialVal) {
        number = intialVal;
    }

    public final synchronized int addAndGet(int delta) {
        number += delta;
        return number;
    }

    public final synchronized int decrementAndGet() {
        --number;
        return number;
    }

    public final synchronized double doubleValue() {
        return (double) number;
    }

    public final synchronized float floatValue() {
        return (float) number;
    }

    public final int get() {
        return number;
    }

    public final synchronized int getAndDecrement() {
        number--;
        return number;
    }

    public final synchronized int getAndIncrement() {
        number++;
        return number;
    }

    public final synchronized int getAndSet(int newValue) {
        int old = number;
        number = newValue;
        return old;
    }

    public final synchronized int incrementAndGet() {
        ++number;
        return number;
    }

    public final synchronized int intValue() {
        return number;
    }

    public final synchronized void set(int newValue) {
        number = newValue;
    }

    public final synchronized long longValue() {
        return (long) number;
    }

    public final synchronized byte byteValue() {
        return (byte) number;
    }

    public final synchronized boolean compareAndSet(int expect, int update) {
        if (number == expect) {
            number = update;
            return true;
        }
        return false;
    }
}
