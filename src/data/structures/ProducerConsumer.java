package data.structures;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ProducerConsumer<E extends Comparable<E>>
        implements Producer<E>, Consumer<E> {
    private static final int THREADS = 4;
    private static final int DEFAULT_CAPACITY = 16;
    private BlockingQueue<E> bQueue;
    private ExecutorService producers;
    private ExecutorService consumers;

    public ProducerConsumer(int maxsize) throws InterruptedException {
        bQueue = new BlockingQueue<>(validateSize(maxsize));
        producers = Executors.newFixedThreadPool(THREADS);
        consumers = Executors.newFixedThreadPool(THREADS);
        consume();
    }

    @Override
    public void consume() throws InterruptedException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    consumers.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                bQueue.poll();
                            } catch (InterruptedException e) {
                                throw new RuntimeException();
                            }
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public void produce(E e) throws InterruptedException {
        producers.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    bQueue.add(e);
                } catch (InterruptedException e) {
                    throw new RuntimeException();
                }
            }
        });
    }

    private int validateSize(int maxSize) {
        return maxSize <= 0 ? DEFAULT_CAPACITY : maxSize;
    }
}