package data.structures;

import java.util.LinkedList;

public class BlockingQueue<E extends Comparable<E>> {
    private LinkedList<E> queue;
    private volatile int maxSize;

    public BlockingQueue(int maxSize) {
        queue = new LinkedList<>();
        this.maxSize = maxSize;
    }

    public boolean isEmpty() {
        return queue.size() == 0;
    }

    public int size() {
        return queue.size();
    }

    public void add(E elem) throws InterruptedException {
        synchronized (this) {
            while (queue.size() == maxSize) {
                wait();
            }
            System.out.println("Producer produced - " + elem);
            queue.add(elem);
            notifyAll();
        }
    }

    // to synchronize block or the whole method
    public synchronized E poll() throws InterruptedException {
        E toRemove = null;
        while (queue.size() == 0) {
            wait();
        }
        toRemove = queue.removeFirst();
        System.out.println("Consumer consumed - " + toRemove);
        notifyAll();
        Thread.sleep(10);
        return toRemove;
    }
}
