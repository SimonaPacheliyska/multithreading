package data.structures;

public interface Producer<E> {
    void produce(E e)throws InterruptedException;
}
