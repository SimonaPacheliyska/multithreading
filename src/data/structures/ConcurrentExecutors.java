package data.structures;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcurrentExecutors implements Consumer<Integer> {
    private final int POOL_SIZE = 4;

    private ConcurrentDS<Integer> data;
    private ExecutorService producers;
    private ExecutorService consumers;
    private static AtomicInteger generator = new AtomicInteger(0);

    public ConcurrentExecutors() throws InterruptedException {
        data = new ConcurrentDS<>();
        producers = Executors.newFixedThreadPool(POOL_SIZE);
        consumers = Executors.newFixedThreadPool(POOL_SIZE);
    }

    @Override
    public void consume() throws InterruptedException {
        consumers.execute(() -> System.out.println("Consumed: " + data.get()));
    }

    public void produce() throws InterruptedException {
        producers.execute(new Runnable() {
            @Override
            public void run() {
                data.put(generator.incrementAndGet());
                System.out.println("Produced: " + generator.get());
            }
        });
    }

    public void process() throws InterruptedException {
        for (int i = 0; i < POOL_SIZE * 10; ++i) {
            produce();
            consume();
        }
    }
}
